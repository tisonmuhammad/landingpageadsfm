$(document).ready(function ($) {
    'use strict';


    /* ---------------------------------------------
         page  Prealoader
     --------------------------------------------- */
    // $(window).on('load', function () {
    //     $("#loading-center").fadeOut();
    //     $("#loading").delay(400).fadeOut("slow");
    // });

    /* ---------------------------------------------
        Sticky header
    --------------------------------------------- */
    $(window).on('scroll', function () {
        var scroll_top = $(window).scrollTop();

        if (scroll_top > 40) {
            $('.navbar').addClass('sticky');

        } else {
            $('.navbar').removeClass('sticky');
        }

    });


    /*--------------------
    VenoBox
     ----------------------*/

    $('.modal-venobox').venobox({
        numeratio: true, // default: false
        infinigall: true,
        titleattr: 'data-title', // default: 'title'// default: false
    });

    /*----------------------------------------------------*/
    /*  VIDEO POP PUP
    /*----------------------------------------------------*/

    $('.video-modal').magnificPopup({
        type: 'iframe',

        iframe: {
            patterns: {
                youtube: {

                    index: 'youtube.com',
                    src: 'https://www.youtube.com/embed/R6Fy6u6cA-I'

                }
            }
        }
    });
    /* ---------------------------------------------
     Back top page scroll up
     --------------------------------------------- */

    $.scrollUp({
        scrollText: '<i class=" arrow_carrot-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });


    /* ---------------------------------------------
     WoW plugin
     --------------------------------------------- */

    new WOW().init({
        mobile: true,
    });

    /* ---------------------------------------------
     Smooth scroll
     --------------------------------------------- */

    $('a.section-scroll[href*="#"]:not([href="#"])').on('click', function (event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') ||
            location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 750);
                return false;
            }
        }
    });

});